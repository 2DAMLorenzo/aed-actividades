package ut4examenlorenzo;

public enum Horario {

    M("Mañana"),
    T("Tarde"),
    N("Noche");

    private String translated;

    Horario(String translated) {
        this.translated = translated;
    }

    @Override
    public String toString() {
        return translated;
    }
}
