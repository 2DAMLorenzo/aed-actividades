package ut4examenlorenzo;

public class Veterinario {

    private String nombre, direccion, horario;

    public Veterinario(String nombre, String direccion, String horario) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.horario = horario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder(nombre + "\n   > Dirección:" + direccion + "\n   > Horario:\n");
        for(int i = 0; i < horario.length(); i+=2) {
            DiaSemana diaSemana = DiaSemana.valueOf(String.valueOf(horario.charAt(i)));
            Horario turno = Horario.valueOf(String.valueOf(horario.charAt(i + 1)));
            res.append("    · ").append(diaSemana.toString()).append(": ").append(turno.toString()).append("\n");
        }
        return res.toString();
    }
}
