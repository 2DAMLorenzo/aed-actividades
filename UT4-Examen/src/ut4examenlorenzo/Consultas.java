package ut4examenlorenzo;

import ut4examenlorenzo.util.DBManager;
import ut4examenlorenzo.util.InvalidObjectException;

public class Consultas {

    public static void main(String[] args) throws InvalidObjectException {
        new DBManager();
        System.out.println("Mostrando todos los datos antes de realizar las modificaciones:");
        printVeterinarios();
        printMascotas();

        System.out.println("\nObteniendo el nombre y teléfono de las mascotas de raza \"Labrador\":");
        for(Mascota mascota : DBManager.queryAll(Mascota.class)) {
            if(mascota.getRaza().trim().equalsIgnoreCase("Labrador")) {
                System.out.println("  - " + mascota.getNombre() + " " + mascota.getTelefono());
            }
        }

        System.out.println("\nModificando especialista de la raza \"Pastor Alemán\" y asignando a \"Florencio\":");

        for(Mascota mascota : DBManager.queryAll(Mascota.class)) {
            if(mascota.getRaza().trim().equalsIgnoreCase("Pastor Aleman")) {
                Veterinario florencio = DBManager.queryByPK(Veterinario.class, "Florencio");
                if(florencio == null) {
                    System.out.println("  No existe ningún veterinario llamado Florencio!!");
                    break;
                }
                mascota.setVeterinario(florencio);
                DBManager.update(mascota);
            }
        }
        printMascotas();

        System.out.println("Mostrando cantidad, telefono y nombre de los Pastores Alemanes:");
        int amount = 0;
        StringBuilder data = new StringBuilder();
        for(Mascota mascota : DBManager.queryAll(Mascota.class)) {
            if(mascota.getRaza().trim().equalsIgnoreCase("Pastor Aleman")) {
                amount++;
                data.append("  - ").append(mascota.getNombre()).append(" ").append(mascota.getTelefono()).append("\n");
            }
        }
        System.out.println("En total hay " + amount + " Pastores Alemanes.\n" + data.toString());

        System.out.println("Eliminando mascota llamada \"Luna\"");
        for(Mascota mascota : DBManager.queryAll(Mascota.class)) {
            if(mascota.getNombre().trim().equalsIgnoreCase("Luna")) {
                System.out.println(DBManager.delete(mascota) ? "  Mascota eliminada con éxito!" :
                        "No existe una mascota llamada Luna.");
            }
        }
        System.out.println();
        printMascotas();

        System.out.println("Mostrando el nombre de los veterinarios que trabajan el Sábado por la noche:");
        for(Veterinario veterinario : DBManager.queryAll(Veterinario.class)) {
            String horario = veterinario.getHorario();
            for(int i = 0; i < horario.length(); i+=2) {
                DiaSemana diaSemana = DiaSemana.valueOf(String.valueOf(horario.charAt(i)));
                Horario turno = Horario.valueOf(String.valueOf(horario.charAt(i + 1)));
                if(diaSemana == DiaSemana.S && turno == Horario.N)
                    System.out.println("  - " + veterinario.getNombre());
            }
        }

        System.out.println("\nMostrando el nombre de la mascota, su veterinario y el horario del mismo.");
        for(Mascota mascota : DBManager.queryAll(Mascota.class)) {
            String horario = mascota.getVeterinario().getHorario();
            StringBuilder parsedHorario = new StringBuilder();
            for(int i = 0; i < horario.length(); i+=2) {
                DiaSemana diaSemana = DiaSemana.valueOf(String.valueOf(horario.charAt(i)));
                Horario turno = Horario.valueOf(String.valueOf(horario.charAt(i + 1)));
                parsedHorario.append("       ").append(diaSemana.toString()).append(": ").append(turno.toString()).append("\n");
            }
            System.out.println("  - Mascota: " + mascota.getNombre() + "\n" +
                    "   > Veterinario: " + mascota.getVeterinario().getNombre() + "\n" +
                    "    · Horario:\n" + parsedHorario.toString());
        }
    }

    private static void printVeterinarios() {
        System.out.println("Veterinarios:");
        for(Veterinario veterinario : DBManager.queryAll(Veterinario.class))
            System.out.println("  - " + veterinario.toString());
    }

    private static void printMascotas() {
        System.out.println("Mascotas:");
        for(Mascota mascota : DBManager.queryAll(Mascota.class))
            System.out.println("  - " + mascota.toString());
    }

}
