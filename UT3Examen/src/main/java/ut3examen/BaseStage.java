package ut3examen;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.Optional;

public class BaseStage {

    protected Optional showAlert(Alert.AlertType type, String content, String header, ButtonType... buttons) {
        Alert alert = new Alert(type, content, buttons);
        alert.setHeaderText(header);
        return alert.showAndWait();
    }

}
