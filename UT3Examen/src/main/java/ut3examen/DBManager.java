package ut3examen;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

public class DBManager {

    private static DBManager instance;

    private SessionFactory sessionFactory;

    public DBManager() {
        instance = this;
        sessionFactory = new Configuration().configure()
                .setProperty("hibernate.connection.username", "root")
                .setProperty("hibernate.connection.password", "").buildSessionFactory();
    }

    public static DBManager getInstance() {
        return instance;
    }

    public <T> void insert(T type) {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.save(type);
        session.getTransaction().commit();
    }

    public <T> void update(T type) {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.update(type);
        session.getTransaction().commit();
    }

    public <T> void delete(T type) {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.delete(type);
        session.getTransaction().commit();
    }

    public <T> List<T> selectAll(Class<T> type) {
        Session session = sessionFactory.getCurrentSession();
        Transaction t = session.beginTransaction();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<T> criteria = builder.createQuery(type);
        criteria.from(type);
        List<T> data = session.createQuery(criteria).getResultList();
        t.commit();
        return data;
    }
}
