package ut3examen.stages;

import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import ut3examen.BaseStage;
import ut3examen.DBManager;
import ut3examen.models.Office;

public class AddOfficeStage extends BaseStage {

    public TextField idField;
    public TextField nameField;
    public TextField addressField;
    public TextField locationField;
    public Button saveButton;
    public Button cancelButton;

    private boolean editMode;
    private Office current;

    public void setEditMode(Office office) {
        editMode = true;
        current = office;

        idField.setText(String.valueOf(office.getId()));
        idField.setDisable(true);
        nameField.setText(office.getName());
        addressField.setText(office.getAddress());
        locationField.setText(office.getLocation());
    }

    public void onSaveClick(ActionEvent actionEvent) {
        if(idField.getText().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, "ID must NOT be NULL!", "Fill the ID field.");
            return;
        }

        if(!editMode) {
            Office office = new Office();
            office.setId(idField.getText());
            office.setName(nameField.getText());
            office.setAddress(addressField.getText());
            office.setLocation(locationField.getText());

            DBManager.getInstance().insert(office);
        } else {
            current.setName(nameField.getText());
            current.setAddress(addressField.getText());
            current.setLocation(locationField.getText());

            DBManager.getInstance().update(current);
        }

        ((Stage)saveButton.getScene().getWindow()).close();
    }

    public void onCancelClick(ActionEvent actionEvent) {
        ((Stage)cancelButton.getScene().getWindow()).close();
    }
}
