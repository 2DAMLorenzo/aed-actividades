package ut1act2;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * Created by thel0w3r on 17/9/18.
 * All Rights Reserved.
 */
public class MainWindow extends JFrame {

    private JTextField dirInputTF;
    private JButton browseBtn;
    private JTable infoTable;
    private DefaultTableModel defaultModel;

    private DirManager dirManager;

    public MainWindow(DirManager dirManager) {
        super("Metadatos de ficheros");

        this.dirManager = dirManager;

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch(ClassNotFoundException | IllegalAccessException | InstantiationException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this,"The system look and feel couldn't be applied or found.");
        }
        this.setSize(600, 480);
        this.getContentPane().setLayout(new GridBagLayout());
        this.setLocationRelativeTo(null);
        setupLayout();
        bindActions();
        refreshTable();
        this.setVisible(true);
    }

    private void setupLayout() {
        dirInputTF = new JTextField(dirManager.getCurrentDir());
        positionElement(0, 0, 2, 1, 1, 0, dirInputTF);
        browseBtn = new JButton("Browse directory");
        positionElement(2, 0, 1, 1, 0, 0, browseBtn);
        //infoTree = new JTree(dirManager.getFiles());
        infoTable = new JTable();
        infoTable.setDragEnabled(false);
        defaultModel = new DefaultTableModel(new String[][]{}, new String[]{"Name", "Type", "Relative path", "Absolute path", "Size", "Permissions"}) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        infoTable.setModel(defaultModel);
        positionElement(0, 1, 3, 1, 1, 1, new JScrollPane(infoTable));
    }

    private void bindActions() {
        browseBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dirManager.setCurrentDir(dirInputTF.getText());
                refreshTable();
            }
        });
    }

    private void positionElement(int x, int y, int xX, int xY, double wX, double wY, JComponent c) {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = x;
        constraints.gridy = y;
        constraints.gridwidth = xX;
        constraints.gridheight = xY;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.weighty = wY;
        constraints.weightx = wX;
        constraints.ipadx = 5;
        constraints.ipady = 5;
        constraints.insets = new Insets(5, 5, 5, 5);
        this.getContentPane().add(c, constraints);
        constraints.weighty = 0.0;
        constraints.weightx = 0.0;
    }

    public void refreshTable() {
        defaultModel.setRowCount(0);
        for(File file : dirManager.getFiles()) {
            defaultModel.addRow(new String[]{
                    file.getName(),
                    file.isFile() ? "File" : "Directory",
                    file.getPath(),
                    file.getAbsolutePath(),
                    Utils.toMegaBytes(file.length()) + " MB",
                    Utils.getPermissions(file)
            });
        }

        defaultModel.fireTableDataChanged();
    }

}
