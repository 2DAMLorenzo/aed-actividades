package ut1act2;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import java.io.File;

/**
 * Created by thel0w3r on 17/9/18.
 * All Rights Reserved.
 */
public class DirManager {

    private String currentDir;

    public DirManager() {
        currentDir = new File(System.getProperty("user.dir")).getAbsolutePath();
    }

    public String getCurrentDir() {
        return currentDir;
    }

    public void setCurrentDir(String newDir) {
        this.currentDir = newDir;
    }

    public File[] getFiles() {
        File path = new File(currentDir);
        if(path.exists() && path.isDirectory()) {
            return path.listFiles();
        } else
            JOptionPane.showMessageDialog(null,"La ruta especificada no existe o es un archivo.", "Error", JOptionPane.ERROR_MESSAGE);
        return null;
    }

}
