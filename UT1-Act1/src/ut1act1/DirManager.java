package ut1act1;

import javax.swing.*;
import java.io.File;

/**
 * Created by thel0w3r on 17/9/18.
 * All Rights Reserved.
 */
public class DirManager {

    private static DirManager instance;

    private String currentDir;

    public DirManager() {
        instance = this;
        currentDir = new File(System.getProperty("user.dir")).getAbsolutePath();
    }

    public static DirManager getInstance() {
        return instance;
    }

    public String getCurrentDir() {
        return currentDir;
    }

    public void setCurrentDir(String newDir) {
        this.currentDir = newDir;
    }

    public DefaultListModel<String> getFiles() {
        DefaultListModel<String> model = new DefaultListModel<>();
        File path = new File(currentDir);
        if(path.exists() && path.isDirectory()) {
            String[] files = path.list();
            if(files != null) {
                for(String file : files) {
                    model.addElement(file);
                }
            }
        } else
            JOptionPane.showMessageDialog(null,"La ruta especificada no existe o es un archivo.", "Error", JOptionPane.ERROR_MESSAGE);
        return model;
    }

}
