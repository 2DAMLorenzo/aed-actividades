package ut1act1;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by thel0w3r on 17/9/18.
 * All Rights Reserved.
 */
public class MainWindow extends JFrame {

    private JTextField dirInputTF;
    private JButton browseBtn;
    private JList<String> dirList;

    private DirManager dirManager;

    public MainWindow() {
        super("Listado de ficheros");

        this.dirManager = DirManager.getInstance();

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch(ClassNotFoundException | IllegalAccessException | InstantiationException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this,"The system look and feel couldn't be applied or found.");
        }
        this.setSize(600, 480);
        this.getContentPane().setLayout(new GridBagLayout());
        this.setLocationRelativeTo(null);
        setupLayout();
        this.setVisible(true);
    }

    private void setupLayout() {
        dirInputTF = new JTextField(dirManager.getCurrentDir());
        positionElement(0, 0, 2, 1, 1, 0, dirInputTF);
        browseBtn = new JButton("Browse directory");
        browseBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dirManager.setCurrentDir(dirInputTF.getText());
                dirList.setModel(dirManager.getFiles());
            }
        });
        positionElement(2, 0, 1, 1, 0, 0, browseBtn);
        dirList = new JList<>();
        dirList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        dirList.setLayoutOrientation(JList.VERTICAL);
        dirList.setModel(dirManager.getFiles());
        positionElement(0, 1, 3, 1, 1, 1, new JScrollPane(dirList));
    }

    private void positionElement(int x, int y, int xX, int xY, double wX, double wY, JComponent c) {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = x;
        constraints.gridy = y;
        constraints.gridwidth = xX;
        constraints.gridheight = xY;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.weighty = wY;
        constraints.weightx = wX;
        constraints.ipadx = 5;
        constraints.ipady = 5;
        constraints.insets = new Insets(5, 5, 5, 5);
        this.getContentPane().add(c, constraints);
        constraints.weighty = 0.0;
        constraints.weightx = 0.0;
    }

}
