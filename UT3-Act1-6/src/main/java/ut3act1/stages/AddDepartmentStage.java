package ut3act1.stages;

import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.stage.Stage;
import ut3act1.DBManager;
import ut3act1.models.Departamentos;

import java.util.Optional;

public class AddDepartmentStage {

    public TextField idField;
    public TextField nameField;
    public TextField locationField;
    public Button saveButton;
    public Button cancelButton;

    private boolean editMode;
    private Departamentos current;

    public void setEditMode(Departamentos department) {
        editMode = true;
        current = department;

        idField.setText(String.valueOf(department.getId()));
        idField.setDisable(true);
        nameField.setText(department.getName());
        locationField.setText(department.getLocation());
    }

    public void onSaveClick(ActionEvent actionEvent) {
        if(idField.getText().isEmpty() || nameField.getText().isEmpty() || locationField.getText().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, "You have to fill all the fields!", "Some fields are empty!", ButtonType.OK);
            return;
        }
        if(!editMode)
            DBManager.getInstance().addDepartment(Integer.valueOf(idField.getText()), nameField.getText(), locationField.getText());
        else {
            current.setName(nameField.getText());
            current.setLocation(locationField.getText());
            DBManager.getInstance().updateDepartment(current);
        }
        ((Stage) saveButton.getScene().getWindow()).close();
    }

    public void onCancelClick(ActionEvent actionEvent) {
        ((Stage) cancelButton.getScene().getWindow()).close();
    }

    private Optional showAlert(Alert.AlertType type, String content, String header, ButtonType... buttons) {
        Alert alert = new Alert(type, content, buttons);
        alert.setHeaderText(header);
        return alert.showAndWait();
    }
}
