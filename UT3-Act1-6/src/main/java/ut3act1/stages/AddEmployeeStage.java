package ut3act1.stages;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.util.Callback;
import ut3act1.DBManager;
import ut3act1.models.Departamentos;
import ut3act1.models.Empleados;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;

public class AddEmployeeStage {

    public TextField idField;
    public TextField nameField;
    public TextField positionField;
    public ComboBox<Departamentos> departmentCB;
    public TextField salaryField;
    public TextField commisionField;
    public DatePicker ingressDateDP;
    public Button saveButton;
    public Button cancelButton;

    private boolean editMode;
    private Empleados current;


    public void initialize() {
        ObservableList<Departamentos> departmentNames = FXCollections.observableArrayList();
        departmentNames.addAll(DBManager.getInstance().loadAllData(Departamentos.class));

        Callback<ListView<Departamentos>, ListCell<Departamentos>> cellFac = new Callback<ListView<Departamentos>, ListCell<Departamentos>>() {
            @Override
            public ListCell<Departamentos> call(ListView<Departamentos> l) {
                return new ListCell<Departamentos>() {
                    @Override
                    protected void updateItem(Departamentos item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item == null || empty) {
                            setGraphic(null);
                        } else {
                            setText(item.getName());
                        }
                    }
                } ;
            }
        };

        departmentCB.setCellFactory(cellFac);
        departmentCB.setButtonCell(cellFac.call(null));
        departmentCB.setItems(departmentNames);
        departmentCB.getSelectionModel().selectFirst();
    }

    public void setEditMode(Empleados employee) {
        editMode = true;
        current = employee;

        idField.setText(String.valueOf(employee.getId()));
        idField.setDisable(true);
        nameField.setText(employee.getName());
        positionField.setText(employee.getPosition());
        departmentCB.getSelectionModel().select(employee.getDepartamento());
        salaryField.setText(String.valueOf(employee.getSalary()));
        commisionField.setText(String.valueOf(employee.getComision()));
        ingressDateDP.setValue(employee.getIngressDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
    }

    public void onSaveClick(ActionEvent actionEvent) {
        if(idField.getText().isEmpty() ||
                nameField.getText().isEmpty() ||
                positionField.getText().isEmpty() ||
                departmentCB.getSelectionModel().getSelectedItem() == null ||
                salaryField.getText().isEmpty() ||
                commisionField.getText().isEmpty() ||
                ingressDateDP.getValue() == null) {
            showAlert(Alert.AlertType.ERROR, "You have to fill all the fields!", "Some fields are empty!", ButtonType.OK);
            return;
        }
        if(!editMode)
            DBManager.getInstance().addEmployee(
                    Integer.valueOf(idField.getText()),
                    nameField.getText(),
                    positionField.getText(),
                    Double.valueOf(salaryField.getText()),
                    Double.valueOf(commisionField.getText()),
                    Date.from(ingressDateDP.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()),
                    departmentCB.getValue()
            );
        else {
            current.setName(nameField.getText());
            current.setPosition(positionField.getText());
            current.setDepartamento(departmentCB.getSelectionModel().getSelectedItem());
            current.setSalary(Double.valueOf(salaryField.getText()));
            current.setComision(Double.valueOf(commisionField.getText()));
            current.setIngressDate(Date.from(ingressDateDP.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()));
            DBManager.getInstance().updateEmployee(current);
        }
        ((Stage)saveButton.getScene().getWindow()).close();
    }

    public void onCancelClick(ActionEvent actionEvent) {
        ((Stage)cancelButton.getScene().getWindow()).close();
    }

    private Optional showAlert(Alert.AlertType type, String content, String header, ButtonType... buttons) {
        Alert alert = new Alert(type, content, buttons);
        alert.setHeaderText(header);
        return alert.showAndWait();
    }
}
