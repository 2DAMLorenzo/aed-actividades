package ut3act1.stages;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import ut3act1.DBManager;
import ut3act1.models.Departamentos;
import ut3act1.models.Empleados;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;
import java.util.Optional;

public class MainStage {

    public TableView<Departamentos> departmentsTable;
    public TableColumn<Departamentos, Integer> depIDCol;
    public TableColumn<Departamentos, String> depNameCol;
    public TableColumn<Departamentos, String> depLocCol;
    public TableView<Empleados> employeesTable;
    public TableColumn<Empleados, Integer> empIDCol;
    public TableColumn<Empleados, String> empNameCol;
    public TableColumn<Empleados, String> empPositionCol;
    public TableColumn<Empleados, String> empDepNameCol;
    public TableColumn<Empleados, Double> empSalaryCol;
    public TableColumn<Empleados, Double> empCommisionCol;
    public TableColumn<Empleados, String> empIngressDateCol;
    public Button deleteSelectedBtn;
    public Button editSelectedBtn;

    private ObservableList<Departamentos> departmentsData = FXCollections.observableArrayList();
    private ObservableList<Empleados> employeesData = FXCollections.observableArrayList();

    public void initialize() {
        depIDCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        depNameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        depLocCol.setCellValueFactory(new PropertyValueFactory<>("location"));
        departmentsTable.setItems(departmentsData);
        departmentsTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Departamentos>() {
            @Override
            public void changed(ObservableValue<? extends Departamentos> observable, Departamentos oldValue, Departamentos newValue) {
                if(newValue != null)
                    employeesTable.getSelectionModel().clearSelection();
            }
        });

        empIDCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        empNameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        empPositionCol.setCellValueFactory(new PropertyValueFactory<>("position"));
        empDepNameCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Empleados, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Empleados, String> param) {
                return new SimpleStringProperty(param.getValue().getDepartamento().getName());
            }
        });
        empSalaryCol.setCellValueFactory(new PropertyValueFactory<>("salary"));
        empCommisionCol.setCellValueFactory(new PropertyValueFactory<>("comision"));
        empIngressDateCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Empleados, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Empleados, String> param) {
                return new SimpleStringProperty(new SimpleDateFormat("dd/MM/yyyy").format(param.getValue().getIngressDate()));
            }
        });
        employeesTable.setItems(employeesData);
        employeesTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Empleados>() {
            @Override
            public void changed(ObservableValue<? extends Empleados> observable, Empleados oldValue, Empleados newValue) {
                if(newValue != null)
                    departmentsTable.getSelectionModel().clearSelection();
            }
        });

        updateTables();
    }

    public void updateTables() {
        departmentsData.clear();
        departmentsData.addAll(DBManager.getInstance().loadAllData(Departamentos.class));

        employeesData.clear();
        employeesData.addAll(DBManager.getInstance().loadAllData(Empleados.class));
    }

    public void onAddDepartmentClick(ActionEvent actionEvent) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/stages/AddDepartmentStage.fxml"));
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setTitle("Añadir departamento");
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(scene);
            stage.showAndWait();
            updateTables();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onAddEmployeeClick(ActionEvent actionEvent) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/stages/AddEmployeeStage.fxml"));
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setTitle("Añadir empleado");
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(scene);
            stage.showAndWait();
            updateTables();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onDeleteSelectedBtnClick(ActionEvent actionEvent) {
        if(departmentsTable.getSelectionModel().getSelectedItem() != null) {
            Collection<Empleados> employees = departmentsTable.getSelectionModel().getSelectedItem().getEmpleados();
            if(employees != null && !employees.isEmpty()) {
                Optional res = showAlert(Alert.AlertType.WARNING, "Hay empleados asignados al departamento seleccionado!\nEliminar el departamento supondría ELIMINAR TODOS LOS EMPLEADOS DEL MISMO!",
                        "Operación potencialmente destructiva", ButtonType.OK, ButtonType.CANCEL);
                if(res.isPresent() && res.get() == ButtonType.OK) {
                    DBManager.getInstance().deleteEmployees(employees);
                    DBManager.getInstance().deleteDepartment(departmentsTable.getSelectionModel().getSelectedItem());
                }
            } else {
                Optional res = showAlert(Alert.AlertType.WARNING, "¿Está seguro que desea eliminar el departamento seleccionado?",
                        "Operación potencialmente destructiva", ButtonType.OK, ButtonType.CANCEL);
                if(res.isPresent() && res.get() == ButtonType.OK) {
                    DBManager.getInstance().deleteDepartment(departmentsTable.getSelectionModel().getSelectedItem());
                }
            }

        } else if(employeesTable.getSelectionModel().getSelectedItem() != null)
            DBManager.getInstance().deleteEmployee(employeesTable.getSelectionModel().getSelectedItem());
        updateTables();
    }

    public void onEditSelectedBtnClick(ActionEvent actionEvent) {
        try {
            if(departmentsTable.getSelectionModel().getSelectedItem() != null) {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/stages/AddDepartmentStage.fxml"));
                Parent root = loader.load();
                AddDepartmentStage controller = loader.getController();
                controller.setEditMode(departmentsTable.getSelectionModel().getSelectedItem());
                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.setTitle("Editar departamento");
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.setScene(scene);
                stage.showAndWait();
            } else if(employeesTable.getSelectionModel().getSelectedItem() != null) {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/stages/AddEmployeeStage.fxml"));
                Parent root = loader.load();
                AddEmployeeStage controller = loader.getController();
                controller.setEditMode(employeesTable.getSelectionModel().getSelectedItem());
                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.setTitle("Editar empleado");
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.setScene(scene);
                stage.showAndWait();
            }
            updateTables();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Optional showAlert(Alert.AlertType type, String content, String header, ButtonType... buttons) {
        Alert alert = new Alert(type, content, buttons);
        alert.setHeaderText(header);
        return alert.showAndWait();
    }
}
