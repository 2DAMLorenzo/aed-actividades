package ut3act1.models;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
public class Empleados {
    private int id;
    private Double comision;
    private Date ingressDate;
    private String name;
    private String position;
    private Double salary;
    private Departamentos departamento;

    @Id
    @Column(name = "ID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "COMISION")
    public Double getComision() {
        return comision;
    }

    public void setComision(Double comision) {
        this.comision = comision;
    }

    @Basic
    @Column(name = "INGRESS_DATE")
    public Date getIngressDate() {
        return ingressDate;
    }

    public void setIngressDate(Date ingressDate) {
        this.ingressDate = ingressDate;
    }

    @Basic
    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "POSITION")
    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Basic
    @Column(name = "SALARY")
    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Empleados empleados = (Empleados) o;
        return id == empleados.id &&
                Objects.equals(comision, empleados.comision) &&
                Objects.equals(ingressDate, empleados.ingressDate) &&
                Objects.equals(name, empleados.name) &&
                Objects.equals(position, empleados.position) &&
                Objects.equals(salary, empleados.salary);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, comision, ingressDate, name, position, salary);
    }

    @ManyToOne
    @JoinColumn(name = "DEPT_ID", referencedColumnName = "ID")
    public Departamentos getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamentos departamento) {
        this.departamento = departamento;
    }
}
