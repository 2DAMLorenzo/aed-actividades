package ut3act1;
/**
 * Created by thel0w3r on 2018-12-17.
 * All Rights Reserved.
 */

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;


public class Main extends Application {



    public static void main(String[] args) {
        new DBManager();
        launch(args);
    }

    public void start(Stage primaryStage) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/stages/MainStage.fxml"));
            Scene scene = new Scene(root);
            primaryStage.setTitle("Hibernate Oracle CRUD");
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
