package ut1act6;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by TheL0w3R on 13/10/2018.
 * All Rights Reserved.
 */
public class PersonManager {

    private ArrayList<Person> persons;

    public PersonManager() {
        persons = new ArrayList<>();
    }

    public Person[] getPersons() {
        return persons.toArray(new Person[]{});
    }

    public void setPersons(Person[] persons) {
        this.persons.addAll(Arrays.asList(persons));
    }

    public void addPerson(Person p) {
        persons.add(p);
    }

    public void removePerson(int index) {
        persons.remove(index);
    }

    public void clearPersons() {
        persons.clear();
    }
}
