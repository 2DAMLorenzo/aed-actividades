package ut1act6;

import java.io.Serializable;

/**
 * Created by TheL0w3R on 13/10/2018.
 * All Rights Reserved.
 */
public class Person implements Serializable {

    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String[] getRaw() {
        return new String[]{name, Integer.toString(age)};
    }
}
