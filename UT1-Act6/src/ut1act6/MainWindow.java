package ut1act6;

import ut1act6.util.Window;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;

/**
 * Created by TheL0w3R on 13/10/2018.
 * All Rights Reserved.
 */
public class MainWindow extends Window {

    private PersonManager personManager;
    private FileManager fileManager;
    private JFileChooser fileChooser;

    private MenuItem newItem;
    private MenuItem openItem;
    private MenuItem saveItem;
    private MenuItem saveAsItem;

    private JButton addBtn;
    private JButton removeBtn;
    private DefaultTableModel defaultModel;
    private JTable personTable;

    public MainWindow() {
        super("Person manager");
        personManager = new PersonManager();
        fileManager = new FileManager();
        fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new FileNameExtensionFilter("Person objects", "persons"));
        fileChooser.setAcceptAllFileFilterUsed(false);
        setupMenu();
        setupLayout();
        setupEventHandlers();
        this.setVisible(true);
    }

    private void setupMenu() {
        MenuBar mainMenu = new MenuBar();
        Menu fileMenu = new Menu("File");
        newItem = new MenuItem("New");
        newItem.setShortcut(new MenuShortcut(KeyEvent.VK_N));
        openItem = new MenuItem("Open");
        openItem.setShortcut(new MenuShortcut(KeyEvent.VK_O));
        saveItem = new MenuItem("Save");
        saveItem.setShortcut(new MenuShortcut(KeyEvent.VK_S));
        saveAsItem = new MenuItem("Save As...");
        saveAsItem.setShortcut(new MenuShortcut(KeyEvent.VK_S, true));

        fileMenu.add(newItem);
        fileMenu.addSeparator();
        fileMenu.add(openItem);
        fileMenu.add(saveItem);
        fileMenu.add(saveAsItem);
        mainMenu.add(fileMenu);
        this.setMenuBar(mainMenu);
    }

    private void setupLayout() {
        JLabel smartLabel = new JLabel("Person database");
        positionElement(0, 0, 2, 1, 1, 0, smartLabel);
        removeBtn = new JButton("Remove");
        positionElement(2, 0, 1, 1, 0, 0, removeBtn);
        addBtn = new JButton("Add");
        positionElement(3, 0, 1, 1, 0, 0, addBtn);
        defaultModel = new DefaultTableModel(new String[][]{}, new String[]{"Name", "Age"}) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        personTable = new JTable(defaultModel);
        positionElement(0, 1, 4, 1, 1, 1, new JScrollPane(personTable));
    }

    private void setupEventHandlers() {
        addBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JTextField name = new JTextField();
                JTextField age = new JTextField();
                JPanel panel = new JPanel(new GridLayout(0, 1));
                panel.add(new JLabel("Name:"));
                panel.add(name);
                panel.add(new JLabel("Age"));
                panel.add(age);
                int result = JOptionPane.showConfirmDialog(null, panel, "Add Person",
                        JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
                if (result == JOptionPane.OK_OPTION) {
                    personManager.addPerson(new Person(name.getText(), Integer.parseInt(age.getText())));
                    updateTable();
                }
            }
        });
        removeBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(personTable.getSelectedRow() != -1) {
                    personManager.removePerson(personTable.getSelectedRow());
                    updateTable();
                }
            }
        });
        newItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fileManager.unsetFile();
                personManager.clearPersons();
                updateTable();
            }
        });
        openItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                chooseFile();
                personManager.setPersons(fileManager.getPersons());
                updateTable();
            }
        });
        saveItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(fileManager.getCurrentFile() == null) {
                    setSaveFile();
                }
                fileManager.savePersons(personManager.getPersons());
            }
        });
        saveAsItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setSaveFile();
                fileManager.savePersons(personManager.getPersons());
            }
        });
    }

    private void updateTable() {
        Person[] persons = personManager.getPersons();
        defaultModel.setRowCount(0);
        for(Person p : persons)
            defaultModel.addRow(p.getRaw());

        defaultModel.fireTableDataChanged();
    }

    private void chooseFile() {
        int returnValue = fileChooser.showOpenDialog(MainWindow.this);
        if(returnValue == JFileChooser.APPROVE_OPTION) {
            fileManager.setFile(fileChooser.getSelectedFile());
        }
    }

    private void setSaveFile() {
        int returnValue = fileChooser.showSaveDialog(MainWindow.this);
        if(returnValue == JFileChooser.APPROVE_OPTION) {
            String filePath = fileChooser.getSelectedFile().getAbsolutePath();
            fileManager.setFile(new File(filePath + ((!filePath.endsWith(".persons")) ? ".persons" : "")));
        }
    }
}
