package ut1act6.util;

import javax.swing.*;
import java.awt.*;

/**
 * Created by thel0w3r on 01/10/2018.
 * All Rights Reserved.
 */
public class Window extends JFrame {

    public Window(String title) {
        this(title, 600, 480);
    }

    public Window(String title, int width, int height) {
        super(title);

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch(ClassNotFoundException | IllegalAccessException | InstantiationException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this,"The system look and feel couldn't be applied or found.");
        }
        this.setSize(width, height);
        this.getContentPane().setLayout(new GridBagLayout());
        this.setLocationRelativeTo(null);
    }

    public void positionElement(int x, int y, int xX, int xY, double wX, double wY, JComponent c) {
        positionElement(x, y, xX, xY, wX, wY, new Insets(5, 5, 5, 5), GridBagConstraints.BOTH, c);
    }

    public void positionElement(int x, int y, int xX, int xY, double wX, double wY, Insets insets, int fill, JComponent c) {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = x;
        constraints.gridy = y;
        constraints.gridwidth = xX;
        constraints.gridheight = xY;
        constraints.fill = fill;
        constraints.weighty = wY;
        constraints.weightx = wX;
        constraints.ipadx = 5;
        constraints.ipady = 5;
        constraints.insets = insets;
        this.getContentPane().add(c, constraints);
    }

}
