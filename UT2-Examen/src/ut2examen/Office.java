package ut2examen;

/**
 * Created by thel0w3r on 2018-12-03.
 * All Rights Reserved.
 */
public class Office {

    private int code;
    private String name, address, location;

    public Office(int code, String name, String address, String location) {
        this.code = code;
        this.name = name;
        this.address = address;
        this.location = location;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getLocation() {
        return location;
    }
}
