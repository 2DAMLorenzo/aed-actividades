package ut2examen.menuoptions;

import com.thel0w3r.tlmenuframework.MenuOption;
import ut2examen.db.DBManager;

import java.io.BufferedReader;
import java.io.File;
import java.time.format.DateTimeFormatter;

/**
 * Created by thel0w3r on 2018-12-03.
 * All Rights Reserved.
 */
public class RunSQL extends MenuOption {

    public RunSQL() {
        super("Ejecutar fichero 'sentencias.sql'");
    }

    @Override
    public void run() {
        DBManager.getInstance().runFile(new File("./sentencias.sql"));
    }
}
