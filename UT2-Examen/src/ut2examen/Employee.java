package ut2examen;

/**
 * Created by thel0w3r on 2018-12-03.
 * All Rights Reserved.
 */
public class Employee {

    private String nif, name, address, position;
    private Office office;

    public Employee(String nif, String name, String address, String position, Office office) {
        this.nif = nif;
        this.name = name;
        this.address = address;
        this.position = position;
        this.office = office;
    }

    public String getNif() {
        return nif;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getPosition() {
        return position;
    }

    public Office getOffice() {
        return office;
    }
}
