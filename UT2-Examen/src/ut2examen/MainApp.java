package ut2examen;/**
 * Created by thel0w3r on 2018-12-03.
 * All Rights Reserved.
 */

import com.thel0w3r.tlmenuframework.Locale;
import com.thel0w3r.tlmenuframework.Menu;
import com.thel0w3r.tlmenuframework.MenuBuilder;
import ut2examen.db.DBConfig;
import ut2examen.db.DBManager;
import ut2examen.menuoptions.RunSQL;
import ut2examen.menuoptions.ShowData;

public class MainApp {

    public static void main(String[] args) {
        new DBConfig();
        new DBManager();

        Menu mainMenu = new MenuBuilder()
                .title("Examen UT2 Lorenzo")
                .setLocale(Locale.ES)
                .exitNum(9)
                .addOption(new ShowData())
                .addOption(new RunSQL())
                .build();
        mainMenu.start();
    }
}
