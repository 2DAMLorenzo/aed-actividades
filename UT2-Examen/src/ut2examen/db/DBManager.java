package ut2examen.db;

import ut2examen.Employee;
import ut2examen.Office;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;

/**
 * Created by thel0w3r on 2018-12-03.
 * All Rights Reserved.
 */
public class DBManager {

    private static DBManager instance;

    private Connection connection;

    public DBManager() {
        instance = this;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://" +
                            DBConfig.getInstance().getProperty("HOST") +
                            ":" + DBConfig.getInstance().getProperty("PORT") +
                            "/" + DBConfig.getInstance().getProperty("DATABASE") +
                            "?useLegacyDatetimeCode=false&serverTimezone=GMT",
                    DBConfig.getInstance().getProperty("USER"),
                    DBConfig.getInstance().getProperty("PASSWORD"));
            createTables();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public static DBManager getInstance() {
        return instance;
    }

    private void createTables() {
        try {
            PreparedStatement offices = connection.prepareStatement("CREATE TABLE IF NOT EXISTS oficinas(cod_ofi INT NOT NULL PRIMARY KEY, nombre VARCHAR(20), direccion VARCHAR(30), localidad VARCHAR(20))");
            offices.executeUpdate();
            PreparedStatement employees = connection.prepareStatement("CREATE TABLE IF NOT EXISTS empleados(nif VARCHAR(9) NOT NULL PRIMARY KEY, nombre VARCHAR(20), direccion VARCHAR(30), cargo VARCHAR(20), cod_ofi INT NOT NULL, CONSTRAINT FK FOREIGN KEY(cod_ofi) REFERENCES oficinas(cod_ofi))");
            employees.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Office[] getOffices() {
        ArrayList<Office> offices = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM oficinas");
            ResultSet rs = ps.executeQuery();
            while(rs.next())
                offices.add(new Office(rs.getInt("cod_ofi"),
                        rs.getString("nombre"), rs.getString("direccion"),
                        rs.getString("localidad")));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return offices.toArray(new Office[]{});
    }

    public Employee[] getEmployees() {
        ArrayList<Employee> employees = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT e.nif, e.nombre, e.cargo, e.direccion, o.cod_ofi, o.nombre AS nombre_ofi, o.direccion AS dir_ofi, o.localidad FROM empleados e, oficinas o WHERE(e.cod_ofi = o.cod_ofi)");
            ResultSet rs = ps.executeQuery();
            while(rs.next())
                employees.add(new Employee(rs.getString("nif"),
                        rs.getString("nombre"), rs.getString("direccion"),
                        rs.getString("cargo"), new Office(
                                rs.getInt("cod_ofi"),
                                rs.getString("nombre_ofi"),
                                rs.getString("dir_ofi"),
                                rs.getString("localidad")
                )));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return employees.toArray(new Employee[]{});
    }

    public void runFile(File file) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while((line = br.readLine()) != null) {
                PreparedStatement ps = connection.prepareStatement(line);
                if(line.startsWith("SELECT")) {
                    ResultSet rs = ps.executeQuery();
                    while (rs.next())
                        System.out.println(rs.getString("nombre") + " - " + rs.getString("direccion"));
                } else
                    ps.executeUpdate();
            }
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
    }
}
