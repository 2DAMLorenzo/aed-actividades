package ut2examen.db;

import java.io.*;
import java.util.Properties;

/**
 * Created by thel0w3r on 2018-12-03.
 * All Rights Reserved.
 */
public class DBConfig {

    private static DBConfig instance;

    private File configFile;
    private Properties config;

    public DBConfig() {
        instance = this;

        configFile = new File("config.properties");

        Properties defaults = new Properties();
        defaults.setProperty("HOST", "localhost");
        defaults.setProperty("PORT", "3306");
        defaults.setProperty("USER", "root");
        defaults.setProperty("PASSWORD", "");
        defaults.setProperty("DATABASE", "ut2_examen");

        config = new Properties(defaults);
        load();
    }

    public static DBConfig getInstance() {
        return instance;
    }

    public String getProperty(String propKey) {
        return config.getProperty(propKey);
    }

    public void setProperty(String key, String value) {
        config.setProperty(key, value);
    }

    public void save() {
        try {
            OutputStream os = new FileOutputStream(configFile);
            config.store(os, "HOST=localhost\n#PORT=3306\n#USER=root\n#PASSWORD=secret\n#DATABASE=ut2_examen");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void load() {
        try {
            if(!configFile.exists()) {
                save();
            }
            FileInputStream fis = new FileInputStream(configFile);
            config.load(fis);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
