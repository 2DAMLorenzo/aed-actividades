package ut1act8;

import ut1act8.util.Window;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;

/**
 * Created by TheL0w3R on 14/10/2018.
 * All Rights Reserved.
 */
public class MainWindow extends Window {

    private FileManager fileManager;
    private JFileChooser fileChooser;

    private JButton openBtn, generateBtn, modifyBtn, searchBtn, exportBtn;
    private DefaultTableModel defaultModel;
    private JTable employeeTable;

    public MainWindow() {
        super("*NO DATA* | School XML");
        fileManager = new FileManager();
        fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new FileNameExtensionFilter("School data", "dat"));
        fileChooser.setAcceptAllFileFilterUsed(false);
        setupLayout();
        setupEventHandlers();
        this.setVisible(true);
    }

    private void setupLayout() {
        JLabel title = new JLabel("School data");
        positionElement(0, 0, 2, 1, 1, 0, title);
        generateBtn = new JButton("Generate");
        positionElement(2, 0, 1, 1, 0, 0, generateBtn);
        openBtn = new JButton("Open");
        positionElement(3, 0, 1, 1, 0, 0, openBtn);
        defaultModel = new DefaultTableModel(new String[][]{}, new String[]{"Code", "Name", "Location"}) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        employeeTable = new JTable(defaultModel);
        positionElement(0, 1, 4, 1, 1, 1, new JScrollPane(employeeTable));
        modifyBtn = new JButton("Modify name");
        positionElement(0, 2, 1, 1, 0, 0, modifyBtn);
        exportBtn = new JButton("Export data");
        positionElement(1, 2, 2, 1, 0, 0, exportBtn);
        searchBtn = new JButton("Search School");
        positionElement(3, 2, 1, 1, 0, 0, searchBtn);
    }

    private void setupEventHandlers() {
        generateBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(fileManager.getCurrentFile() == null)
                    setSaveFile();

                ArrayList<School> schools = new ArrayList<>();
                schools.add(new School(7564, "El rincón", "Las Palmas"));
                schools.add(new School(984, "Villa de Agüimes", "Agüimes"));
                schools.add(new School(165, "Motor Grande", "Mogán"));
                schools.add(new School(43, "José Zerpa", "Vecindario"));
                schools.add(new School(664, "IES Ingenio", "Ingenio"));
                schools.add(new School(664, "Joaquín Artiles", "Agüimes"));
                fileManager.save(schools.toArray(new School[]{}));
            }
        });
        openBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                chooseFile();
                defaultModel.setRowCount(0);
            }
        });
        searchBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(fileManager.getCurrentFile() != null) {
                    String value = JOptionPane.showInputDialog(MainWindow.this, "Enter the school code to search for:");
                    try {
                        School sc = fileManager.searchSchool(Integer.valueOf(value));
                        if(sc != null)
                            updateTable(sc);
                        else
                            JOptionPane.showMessageDialog(MainWindow.this, "The School code you specified doesn't exist!", "Unknown School", JOptionPane.ERROR_MESSAGE);
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(MainWindow.this, "The School code must be a valid Integer!", "Invalid input", JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(MainWindow.this, "There is no opened file." +
                            "\r\nTo search schools you'll need to select a file containing school data." +
                            "\r\nYou can generate such file by using the \"Generate\" button.", "No data to search", JOptionPane.WARNING_MESSAGE);
                }
            }
        });
        modifyBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(fileManager.getCurrentFile() != null) {
                    if(employeeTable.getRowCount() < 1)
                        JOptionPane.showMessageDialog(MainWindow.this, "There is no school selected." +
                                "\r\nTo to select a school, click on the search button and enter a valid Code.", "No School selected", JOptionPane.WARNING_MESSAGE);
                    else {
                        String value = JOptionPane.showInputDialog(MainWindow.this, "Enter the new name:");
                        int schoolCode = Integer.valueOf((String)defaultModel.getValueAt(0, 0));
                        if(value.length() <= 20) {
                            fileManager.modifyName(schoolCode, value);
                            updateTable(fileManager.searchSchool(schoolCode));
                        } else
                            JOptionPane.showMessageDialog(MainWindow.this, "The value must be 20 characters or less!", "Invalid input", JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(MainWindow.this, "There is no opened file." +
                            "\r\nTo search employees you'll need to select a file containing employee's data." +
                            "\r\nYou can generate such file by using the \"Generate\" button.", "No data to search", JOptionPane.WARNING_MESSAGE);
                }
            }
        });
        exportBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(fileManager.getCurrentFile() != null) {
                    fileChooser.setFileFilter(new FileNameExtensionFilter("XML files", "xml"));
                    int returnValue = fileChooser.showSaveDialog(MainWindow.this);
                    if(returnValue == JFileChooser.APPROVE_OPTION) {
                        String filePath = fileChooser.getSelectedFile().getAbsolutePath();
                        JComboBox<String> exportMethodSelector = new JComboBox<>();
                        exportMethodSelector.addItem("DOM");
                        exportMethodSelector.addItem("XStream");
                        JPanel panel = new JPanel(new GridLayout(0, 1));
                        panel.add(new JLabel("Select an export method:"));
                        panel.add(exportMethodSelector);
                        int result = JOptionPane.showConfirmDialog(null, panel, "Export data",
                                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
                        if (result == JOptionPane.OK_OPTION) {
                            try {
                                String method = (String)exportMethodSelector.getSelectedItem();
                                File file = new File(filePath + ((!filePath.endsWith(".xml")) ? ".xml" : ""));
                                if(method.equalsIgnoreCase("DOM"))
                                    fileManager.exportXML(file);
                                else
                                    fileManager.exportXstream(file);
                            } catch (NumberFormatException ex) {
                                JOptionPane.showMessageDialog(null, "Price must be a valid number!", "Invalid input", JOptionPane.ERROR_MESSAGE);
                            }
                        }
                    }
                    fileChooser.setFileFilter(new FileNameExtensionFilter("School data", "dat"));
                } else {
                    JOptionPane.showMessageDialog(MainWindow.this, "There is no opened file." +
                            "\r\nTo export data, please, open a file first.", "No data to search", JOptionPane.WARNING_MESSAGE);
                }
            }
        });
    }

    private void setSaveFile() {
        int returnValue = fileChooser.showSaveDialog(MainWindow.this);
        if(returnValue == JFileChooser.APPROVE_OPTION) {
            String filePath = fileChooser.getSelectedFile().getAbsolutePath();
            fileManager.setFile(new File(filePath + ((!filePath.endsWith(".dat")) ? ".dat" : "")));
        }
    }

    private void chooseFile() {
        int returnValue = fileChooser.showOpenDialog(MainWindow.this);
        if(returnValue == JFileChooser.APPROVE_OPTION) {
            fileManager.setFile(fileChooser.getSelectedFile());
            this.setTitle(fileChooser.getSelectedFile().getName() + " | School XML");
        }
    }

    private void updateTable(School s) {
        defaultModel.setRowCount(0);
        defaultModel.addRow(s.getRaw());
        defaultModel.fireTableDataChanged();
    }

}
