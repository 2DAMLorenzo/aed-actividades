package ut1act8;

/**
 * Created by TheL0w3R on 14/10/2018.
 * All Rights Reserved.
 */
public class School {

    private int code;
    private String name;
    private String location;

    public School(int code, String name, String location) {
        this.code = code;
        this.name = name;
        this.location = location;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String[] getRaw() {
        return new String[]{String.valueOf(code), name, location};
    }
}
