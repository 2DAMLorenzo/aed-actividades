package ut5act3;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by TheL0w3R on 06/02/2019.
 * All Rights Reserved.
 */
public class MainApp extends Application {

    public static void main(String[] args) {
        new DataManager();
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/MainStage.fxml"));
            Scene scene = new Scene(root);
            primaryStage.setScene(scene);
            primaryStage.setTitle("JSON User Database");
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
