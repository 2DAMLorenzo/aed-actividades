package ut5act3;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.Optional;

/**
 * Created by TheL0w3R on 06/02/2019.
 * All Rights Reserved.
 */
public class FXUtils {

    public static Optional showAlert(Alert.AlertType type, String content, String header, ButtonType... buttons) {
        Alert alert = new Alert(type, content, buttons);
        alert.setHeaderText(header);
        return alert.showAndWait();
    }
}
