package ut1act7;

/**
 * Created by TheL0w3R on 14/10/2018.
 * All Rights Reserved.
 */
public class Employee {

    private int id;
    private String name;
    private String phone;
    private double salary;

    public Employee(int id, String name, String phone, double salary) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String[] getRaw() {
        return new String[]{String.valueOf(id), name, phone, String.valueOf(salary)};
    }
}
