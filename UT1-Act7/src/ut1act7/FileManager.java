package ut1act7;

import javax.swing.*;
import java.io.*;

/**
 * Created by TheL0w3R on 14/10/2018.
 * All Rights Reserved.
 */
public class FileManager {

    private File currentFile;

    public File getCurrentFile() {
        return currentFile;
    }

    public void setFile(File f) {
        this.currentFile = f;
    }

    public void save(Employee[] employees) {
        try {
            RandomAccessFile raf = new RandomAccessFile(currentFile, "rw");
            StringBuffer sb = null;
            for(Employee e : employees) {
                raf.writeInt(e.getId());
                sb = new StringBuffer(e.getName());
                sb.setLength(25); //25 characters, 400 bits, 50 bytes.
                raf.writeChars(sb.toString());
                sb = new StringBuffer(e.getPhone());
                sb.setLength(9); //9 characters, 144 bits, 18 bytes.
                raf.writeChars(sb.toString());
                raf.writeDouble(e.getSalary());
            }
            raf.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "An error has occurred on the file writing procedure.\r\n" + e.getMessage(), "Error writing file", JOptionPane.ERROR_MESSAGE);
        }
    }

    public Employee searchEmployee(int id) {
        try {
            RandomAccessFile raf = new RandomAccessFile(currentFile, "r");
            int pos = 0;
            try {
                while(true) {
                    // 4 + 50 + 18 + 8
                    raf.seek(pos);
                    int currentID = raf.readInt();
                    if(id == currentID) {
                        char[] name = new char[25];
                        for(int i = 0; i < name.length; i++) {
                            name[i] = raf.readChar();
                        }
                        char[] phone = new char[9];
                        for(int i = 0; i < phone.length; i++) {
                            phone[i] = raf.readChar();
                        }
                        double salary = raf.readDouble();

                        return new Employee(currentID, new String(name), new String(phone), salary);
                    }
                    pos += 80;
                }
            } catch (EOFException ignored) {}
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "An error has occurred on the file reading procedure.\r\n" + e.getMessage(), "Error reading file", JOptionPane.ERROR_MESSAGE);
        }
        return null;
    }

    public void modifySalary(int id, double salary) {
        try {
            RandomAccessFile raf = new RandomAccessFile(currentFile, "rw");
            int pos = 0;
            try {
                while(true) {
                    // 4 + 50 + 18 + 8 = total employee record
                    raf.seek(pos);
                    int currentID = raf.readInt();
                    if(id == currentID) {
                        pos += 72; // Skipping the ID, name and phone fields. (80 - 8)
                        raf.seek(pos);
                        raf.writeDouble(salary);
                    }
                    pos += 80;
                }
            } catch (EOFException ignored) {}
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "An error has occurred on the file reading procedure.\r\n" + e.getMessage(), "Error reading file", JOptionPane.ERROR_MESSAGE);
        }
    }

}
