package ut2act17.database;

import ut2act17.Song;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by TheL0w3R on 27/11/2018.
 * All Rights Reserved.
 */
public class DatabaseManager {

    private static DatabaseManager instance;

    private DatabaseConfig config;
    private DatabaseQueries queries;
    private Connection connection;
    private boolean success;

    public DatabaseManager() {
        instance = this;
        success = false;
        config = new DatabaseConfig();
        init();
    }

    public static DatabaseManager getInstance() {
        return instance;
    }

    public boolean isSuccess() {
        return success;
    }

    public void init() {
        try {
            EngineProperties dbprops = EngineProperties.getPropertiesFor(
                    DatabaseEngine.valueOf(config.getProperty("DBENGINE")));
            queries = new DatabaseQueries(DatabaseEngine.valueOf(config.getProperty("DBENGINE")));
            Class.forName(dbprops.getDriverClass());
            connection = dbprops.getConnection();
            success = true;
            createTables();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    private void createTables() {
        try {
            PreparedStatement statement = connection.prepareStatement(queries.getQuery("CREATE").replaceAll(
                    "#workspace#", DatabaseConfig.getInstance().getProperty("ORACLE_WORKSPACE")));
            statement.executeUpdate();
        } catch (SQLException e) {
            if(DatabaseConfig.getInstance().getProperty("DBENGINE").equalsIgnoreCase(DatabaseEngine.DERBY.toString())) {
                if(e.getSQLState().equals("X0Y32")) {
                    return;
                }
            } else if(DatabaseConfig.getInstance().getProperty("DBENGINE").equalsIgnoreCase(DatabaseEngine.ORACLE.toString())) {
                if(e.getErrorCode() == 955) {
                    return;
                }
            }
            e.printStackTrace();
        }
    }

    public Song[] getAllSongs() {
        ArrayList<Song> songs = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement(queries.getQuery("SELECT_ALL").replaceAll("#workspace#",
                    DatabaseConfig.getInstance().getProperty("ORACLE_WORKSPACE")));
            ResultSet result = ps.executeQuery();

            while (result.next())
                songs.add(new Song(result.getInt("id"),
                        result.getString("name"),
                        result.getString("artist"),
                        result.getString("genere")));

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return songs.toArray(new Song[]{});
    }

    public void insertSong(Song song) {
        try {
            PreparedStatement ps = connection.prepareStatement(queries.getQuery("INSERT"));
            ps.setString(1, song.getName());
            ps.setString(2, song.getArtist());
            ps.setString(3, song.getGenere());
            if(DatabaseConfig.getInstance().getProperty("DBENGINE").equalsIgnoreCase(DatabaseEngine.ORACLE.toString())) {
                ps.setInt(4, getLastID() + 1);
            }
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int getLastID() {
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT id FROM ROOT.SONGS WHERE id = (SELECT MAX(id) FROM ROOT.SONGS)");
            ResultSet rs = ps.executeQuery();
            if(rs.next())
                return rs.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }


    public void updateSong(Song song) {
        try {
            PreparedStatement ps = connection.prepareStatement(queries.getQuery("UPDATE"));
            ps.setString(1, song.getName());
            ps.setString(2, song.getArtist());
            ps.setString(3, song.getGenere());
            ps.setInt(4, song.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteSong(int id) {
        try {
            PreparedStatement ps = connection.prepareStatement(queries.getQuery("DELETE"));
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Song getSongByID(int id) {
        try {
            PreparedStatement ps = connection.prepareStatement(queries.getQuery("SELECT_BY_ID"));
            ps.setInt(1, id);
            ResultSet result = ps.executeQuery();
            while (result.next())
                return new Song(result.getInt("id"),
                        result.getString("name"),
                        result.getString("artist"),
                        result.getString("genere"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
