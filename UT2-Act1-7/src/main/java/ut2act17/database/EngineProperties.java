package ut2act17.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by TheL0w3R on 27/11/2018.
 * All Rights Reserved.
 */
public class EngineProperties {

    private String driverClass;
    private Connection connection;

    public EngineProperties(String driverClass, Connection connection) {
        this.driverClass = driverClass;
        this.connection = connection;
    }

    public String getDriverClass() {
        return driverClass;
    }

    public Connection getConnection() {
        return connection;
    }

    public static EngineProperties getPropertiesFor(DatabaseEngine engine) throws SQLException, ClassNotFoundException {
        String driverClass = "";
        Connection conn = null;
        switch(engine) {
            case SQLITE:
                driverClass = "org.sqlite.JDBC";
                conn = DriverManager.getConnection("jdbc:sqlite:" + DatabaseConfig.getInstance().getProperty("SQLITE_DBPATH"));
                break;
            case MYSQL:
                driverClass = "com.mysql.cj.jdbc.Driver";
                conn = DriverManager.getConnection("jdbc:mysql://" + DatabaseConfig.getInstance().getProperty("MYSQL_HOST") + ":" +
                                DatabaseConfig.getInstance().getProperty("MYSQL_PORT") + "/" +
                                DatabaseConfig.getInstance().getProperty("MYSQL_DATABASE") +
                                "?useLegacyDatetimeCode=false&serverTimezone=GMT",
                        DatabaseConfig.getInstance().getProperty("MYSQL_USER"),
                        DatabaseConfig.getInstance().getProperty("MYSQL_PASSWORD"));
                break;
            case DERBY:
                driverClass = "org.apache.derby.jdbc.ClientDriver";
                conn = DriverManager.getConnection("jdbc:derby://" + DatabaseConfig.getInstance().getProperty("DERBY_HOST") + ":" +
                        DatabaseConfig.getInstance().getProperty("DERBY_PORT") + "/" + DatabaseConfig.getInstance().getProperty("DERBY_DATABASE") +
                        ";create=true");
                break;
            case ORACLE:
                driverClass = "oracle.jdbc.driver.OracleDriver";
                conn = DriverManager.getConnection("jdbc:oracle:thin:@//" + DatabaseConfig.getInstance().getProperty("ORACLE_HOST") + ":" +
                                DatabaseConfig.getInstance().getProperty("ORACLE_PORT"),
                        DatabaseConfig.getInstance().getProperty("ORACLE_USER"),
                        DatabaseConfig.getInstance().getProperty("ORACLE_PASSWORD"));
                conn.setAutoCommit(true);
                break;
            default:
                return null;
        }

        return new EngineProperties(driverClass, conn);
    }

}
