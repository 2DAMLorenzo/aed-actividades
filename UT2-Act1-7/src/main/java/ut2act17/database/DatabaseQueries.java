package ut2act17.database;

import java.util.HashMap;

/**
 * Created by TheL0w3R on 27/11/2018.
 * All Rights Reserved.
 */
public class DatabaseQueries {

    private DatabaseEngine engine;
    private HashMap<DatabaseEngine, HashMap> allQueries;

    public DatabaseQueries(DatabaseEngine engine) {
        this.engine = engine;
        allQueries = new HashMap<>();

        HashMap<String, String> sqliteQueries = new HashMap<>();
        HashMap<String, String> mysqlQueries = new HashMap<>();
        HashMap<String, String> derbyQueries = new HashMap<>();
        HashMap<String, String> oracleQueries = new HashMap<>();

        sqliteQueries.put("CREATE", "CREATE TABLE IF NOT EXISTS songs (id INTEGER PRIMARY KEY AUTOINCREMENT, name STRING, artist STRING, genere STRING)");
        mysqlQueries.put("CREATE", "CREATE TABLE IF NOT EXISTS songs (id INTEGER PRIMARY KEY AUTO_INCREMENT, name VARCHAR(100), artist VARCHAR(100), genere VARCHAR(50))");
        derbyQueries.put("CREATE", "CREATE TABLE songs (id INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), name VARCHAR(100), artist VARCHAR(100), genere VARCHAR(50))");
        oracleQueries.put("CREATE", "CREATE TABLE #workspace#.songs (id INTEGER NOT NULL PRIMARY KEY, name VARCHAR(100) NOT NULL, artist VARCHAR(100) NOT NULL, genere VARCHAR(50) NOT NULL)");

        sqliteQueries.put("INSERT", "INSERT INTO songs(name, artist, genere) VALUES(?, ?, ?)");
        mysqlQueries.put("INSERT", "INSERT INTO songs(name, artist, genere) VALUES(?, ?, ?)");
        derbyQueries.put("INSERT", "INSERT INTO songs(name, artist, genere) VALUES(?, ?, ?)");
        oracleQueries.put("INSERT", "INSERT INTO songs(name, artist, genere, id) VALUES(?, ?, ?, ?)");

        sqliteQueries.put("UPDATE", "UPDATE songs SET name = ?, artist = ?, genere = ? WHERE id = ?");
        mysqlQueries.put("UPDATE", "UPDATE songs SET name = ?, artist = ?, genere = ? WHERE id = ?");
        derbyQueries.put("UPDATE", "UPDATE songs SET name = ?, artist = ?, genere = ? WHERE id = ?");
        oracleQueries.put("UPDATE", "UPDATE songs SET name = ?, artist = ?, genere = ? WHERE id = ?");

        sqliteQueries.put("DELETE", "DELETE FROM songs where id = ?");
        mysqlQueries.put("DELETE", "DELETE FROM songs where id = ?");
        derbyQueries.put("DELETE", "DELETE FROM songs where id = ?");
        oracleQueries.put("DELETE", "DELETE FROM songs where id = ?");

        sqliteQueries.put("SELECT_BY_ID", "SELECT * FROM songs WHERE id = ?");
        mysqlQueries.put("SELECT_BY_ID", "SELECT * FROM songs WHERE id = ?");
        derbyQueries.put("SELECT_BY_ID", "SELECT * FROM songs WHERE id = ?");
        oracleQueries.put("SELECT_BY_ID", "SELECT * FROM songs WHERE id = ?");

        sqliteQueries.put("SELECT_ALL", "SELECT * FROM songs");
        mysqlQueries.put("SELECT_ALL", "SELECT * FROM songs");
        derbyQueries.put("SELECT_ALL", "SELECT * FROM songs");
        oracleQueries.put("SELECT_ALL", "SELECT * FROM #workspace#.songs ORDER BY id");

        allQueries.put(DatabaseEngine.SQLITE, sqliteQueries);
        allQueries.put(DatabaseEngine.MYSQL, mysqlQueries);
        allQueries.put(DatabaseEngine.DERBY, derbyQueries);
        allQueries.put(DatabaseEngine.ORACLE, oracleQueries);
    }

    public String getQuery(String query) {
        return (String) allQueries.get(engine).get(query);
    }
}
