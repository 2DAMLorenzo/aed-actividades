package ut2act17.stages;

import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import ut2act17.Song;
import ut2act17.database.DatabaseManager;

import java.util.Optional;

/**
 * Created by TheL0w3R on 27/11/2018.
 * All Rights Reserved.
 */
public class AddEditStageController {

    public Button cancelButton;
    public Button saveButton;
    public TextField nameField;
    public TextField artistField;
    public TextField genereField;

    private boolean editMode = false;
    private Song currentSong = null;

    public void setEditMode(Song song) {
        editMode = true;
        currentSong = song;
        nameField.setText(song.getName());
        artistField.setText(song.getArtist());
        genereField.setText(song.getGenere());
    }

    public void onCancelButtonClick(ActionEvent actionEvent) {
        ((Stage) cancelButton.getScene().getWindow()).close();
    }

    public void onSaveButtonClick(ActionEvent actionEvent) {
        if(nameField.getText().isEmpty() || artistField.getText().isEmpty() || genereField.getText().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, "You have to fill all the fields!", "Some fields are empty!", ButtonType.OK);
            return;
        }

        if(!editMode)
            DatabaseManager.getInstance().insertSong(new Song(nameField.getText(), artistField.getText(), genereField.getText()));
        else
            DatabaseManager.getInstance().updateSong(new Song(currentSong.getId(), nameField.getText(), artistField.getText(), genereField.getText()));

        ((Stage) cancelButton.getScene().getWindow()).close();
    }

    private Optional showAlert(Alert.AlertType type, String content, String header, ButtonType... buttons) {
        Alert alert = new Alert(type, content, buttons);
        alert.setHeaderText(header);
        return alert.showAndWait();
    }
}
