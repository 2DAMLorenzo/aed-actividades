package ut2act17.stages;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import ut2act17.FileManager;
import ut2act17.Song;
import ut2act17.database.DatabaseConfig;
import ut2act17.database.DatabaseEngine;
import ut2act17.database.DatabaseManager;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

/**
 * Created by thel0w3r on 2018-11-26.
 * All Rights Reserved.
 */
public class MainStageController {

    public MenuItem preferencesMenuItem;
    public TableView<Song> musicTable;
    public TableColumn<Song, Integer> idColumn;
    public TableColumn<Song, String> nameColumn;
    public TableColumn<Song, String> artistColumn;
    public TableColumn<Song, String> genereColumn;
    public Button deleteButton;
    public Button editButton;
    public Button addButton;
    public MenuItem importMenuItem;
    public MenuItem exportMenuItem;

    private ObservableList<Song> tableData = FXCollections.observableArrayList();

    public void initialize() {
        idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        artistColumn.setCellValueFactory(new PropertyValueFactory<>("artist"));
        genereColumn.setCellValueFactory(new PropertyValueFactory<>("genere"));

        musicTable.setItems(tableData);
        updateData();
    }

    public void onPreferencesMenuItemClick(ActionEvent actionEvent) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/stages/PreferencesStage.fxml"));
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setTitle("Preferences");
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(scene);
            stage.showAndWait();
            ((Stage)addButton.getScene().getWindow()).setTitle("MultiDB Music Library | " + DatabaseEngine.valueOf(DatabaseConfig.getInstance().getProperty("DBENGINE")));
            updateData();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onAddButtonClick(ActionEvent actionEvent) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/stages/AddEditStage.fxml"));
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setTitle("Add song");
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(scene);
            stage.showAndWait();
            updateData();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onEditButtonClick(ActionEvent actionEvent) {
        if(musicTable.getSelectionModel().getSelectedIndex() == -1) {
            showAlert(Alert.AlertType.ERROR, "Make sure to select a song before editing it.", "There is no song selected!");
            return;
        }
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/stages/AddEditStage.fxml"));
            Parent root = loader.load();
            AddEditStageController controller = loader.getController();
            controller.setEditMode(DatabaseManager.getInstance().getSongByID(musicTable.getSelectionModel().getSelectedItem().getId()));
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setTitle("Edit song");
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(scene);
            stage.showAndWait();
            updateData();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onDeleteButtonClick(ActionEvent actionEvent) {
        if(musicTable.getSelectionModel().getSelectedIndex() == -1) {
            showAlert(Alert.AlertType.ERROR, "Make sure to select a song before editing it.", "There is no song selected!");
            return;
        }

        Optional res = showAlert(Alert.AlertType.WARNING,
                "Do you really want to delete the selected song?",
                "You're deleting a question!",
                ButtonType.CANCEL, ButtonType.OK
        );

        if(res.isPresent() && res.get() == ButtonType.OK) {
            DatabaseManager.getInstance().deleteSong(musicTable.getSelectionModel().getSelectedItem().getId());
            updateData();
        }
    }

    private Optional showAlert(Alert.AlertType type, String content, String header, ButtonType... buttons) {
        Alert alert = new Alert(type, content, buttons);
        alert.setHeaderText(header);
        return alert.showAndWait();
    }

    private void updateData() {
        if(DatabaseManager.getInstance().isSuccess()) {
            tableData.clear();
            tableData.addAll(Arrays.asList(DatabaseManager.getInstance().getAllSongs()));
        }
    }

    public void onImportMenuItemClick(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("MultiDB data", "*.multidb"));
        File selectedFile = fileChooser.showOpenDialog(addButton.getScene().getWindow());
        if(selectedFile != null) {
            try {
                Song[] songs = FileManager.loadSongs(selectedFile);

                Optional res = showAlert(Alert.AlertType.INFORMATION,
                        "Do you want to continue?",
                        "The file contains " + songs.length + " songs",
                        ButtonType.CANCEL, ButtonType.OK
                );

                if(res.isPresent() && res.get() == ButtonType.OK) {
                    for(Song s : songs) {
                        DatabaseManager.getInstance().insertSong(s);
                    }
                    updateData();
                }
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public void onExportMenuItemClick(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("MultiDB data", "*.multidb"));
        File selectedFile = fileChooser.showSaveDialog(addButton.getScene().getWindow());
        if(selectedFile != null) {
            System.out.println(selectedFile.getAbsoluteFile());
            try {
                FileManager.saveSongs(selectedFile, DatabaseManager.getInstance().getAllSongs());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
