package ut2act17.stages;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import ut2act17.database.DatabaseConfig;
import ut2act17.database.DatabaseEngine;
import ut2act17.database.DatabaseManager;

import java.util.Arrays;

/**
 * Created by TheL0w3R on 27/11/2018.
 * All Rights Reserved.
 */
public class PreferencesStageController {


    public ComboBox<String> dbEngineCombo;
    public TextField sqliteDbPath;
    public TextField mysqlHost;
    public TextField mysqlUser;
    public TextField mysqlPort;
    public TextField mysqlDatabase;
    public PasswordField mysqlPassword;
    public TextField oracleHost;
    public TextField oracleUser;
    public TextField oraclePort;
    public TextField oracleWorkspace;
    public PasswordField oraclePassword;
    public Button saveButton;
    public TextField derbyHost;
    public TextField derbyPort;
    public TextField derbyDatabase;

    public void initialize() {
        dbEngineCombo.setItems(FXCollections.observableArrayList(Arrays.stream(DatabaseEngine.values())
                .map(DatabaseEngine::toString)
                .toArray(String[]::new)
        ));

        dbEngineCombo.getSelectionModel().select(DatabaseEngine.valueOf(DatabaseConfig.getInstance().getProperty("DBENGINE")).toString());

        sqliteDbPath.setText(DatabaseConfig.getInstance().getProperty("SQLITE_DBPATH"));

        mysqlHost.setText(DatabaseConfig.getInstance().getProperty("MYSQL_HOST"));
        mysqlUser.setText(DatabaseConfig.getInstance().getProperty("MYSQL_USER"));
        mysqlPort.setText(DatabaseConfig.getInstance().getProperty("MYSQL_PORT"));
        mysqlDatabase.setText(DatabaseConfig.getInstance().getProperty("MYSQL_DATABASE"));
        mysqlPassword.setText(DatabaseConfig.getInstance().getProperty("MYSQL_PASSWORD"));

        oracleHost.setText(DatabaseConfig.getInstance().getProperty("ORACLE_HOST"));
        oracleUser.setText(DatabaseConfig.getInstance().getProperty("ORACLE_USER"));
        oraclePort.setText(DatabaseConfig.getInstance().getProperty("ORACLE_PORT"));
        oracleWorkspace.setText(DatabaseConfig.getInstance().getProperty("ORACLE_WORKSPACE"));
        oraclePassword.setText(DatabaseConfig.getInstance().getProperty("ORACLE_PASSWORD"));

        derbyHost.setText(DatabaseConfig.getInstance().getProperty("DERBY_HOST"));
        derbyPort.setText(DatabaseConfig.getInstance().getProperty("DERBY_PORT"));
        derbyDatabase.setText(DatabaseConfig.getInstance().getProperty("DERBY_DATABASE"));
    }

    public void onSaveButton(ActionEvent actionEvent) {
        DatabaseConfig.getInstance().setProperty("DBENGINE", dbEngineCombo.getSelectionModel().getSelectedItem().toUpperCase());
        DatabaseConfig.getInstance().setProperty("SQLITE_DBPATH", sqliteDbPath.getText());
        DatabaseConfig.getInstance().setProperty("MYSQL_HOST", mysqlHost.getText());
        DatabaseConfig.getInstance().setProperty("MYSQL_USER", mysqlUser.getText());
        DatabaseConfig.getInstance().setProperty("MYSQL_PORT", mysqlPort.getText());
        DatabaseConfig.getInstance().setProperty("MYSQL_DATABASE", mysqlDatabase.getText());
        DatabaseConfig.getInstance().setProperty("MYSQL_PASSWORD", mysqlPassword.getText());
        DatabaseConfig.getInstance().setProperty("ORACLE_HOST", oracleHost.getText());
        DatabaseConfig.getInstance().setProperty("ORACLE_USER", oracleUser.getText());
        DatabaseConfig.getInstance().setProperty("ORACLE_PORT", oraclePort.getText());
        DatabaseConfig.getInstance().setProperty("ORACLE_WORKSPACE", oracleWorkspace.getText());
        DatabaseConfig.getInstance().setProperty("ORACLE_PASSWORD", oraclePassword.getText());
        DatabaseConfig.getInstance().setProperty("DERBY_HOST", derbyHost.getText());
        DatabaseConfig.getInstance().setProperty("DERBY_PORT", derbyPort.getText());
        DatabaseConfig.getInstance().setProperty("DERBY_DATABASE", derbyDatabase.getText());
        DatabaseConfig.getInstance().save();
        DatabaseManager.getInstance().init();
        ((Stage) saveButton.getScene().getWindow()).close();
    }
}
