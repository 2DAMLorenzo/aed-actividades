package ut2act17;

import java.io.Serializable;

/**
 * Created by TheL0w3R on 27/11/2018.
 * All Rights Reserved.
 */
public class Song implements Serializable {

    private int id;
    private String name, artist, genere;

    public Song(String name, String artist, String genere) {
        this.id = 0;
        this.name = name;
        this.artist = artist;
        this.genere = genere;
    }

    public Song(int id, String name, String artist, String genere) {
        this.id = id;
        this.name = name;
        this.artist = artist;
        this.genere = genere;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getArtist() {
        return artist;
    }

    public String getGenere() {
        return genere;
    }
}
