package ut2act17;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by TheL0w3R on 28/11/2018.
 * All Rights Reserved.
 */
public class FileManager {

    public static void saveSongs(File out, Song[] songs) throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(out));

        for(Song s : songs)
            oos.writeObject(s);

        oos.flush();
        oos.close();
    }

    public static Song[] loadSongs(File in) throws IOException, ClassNotFoundException {
        ArrayList<Song> songs = new ArrayList<>();

        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(in));
        try {
            while(true) {
                songs.add((Song) ois.readObject());
            }
        } catch(EOFException ignored){}

        return songs.toArray(new Song[]{});
    }

}
