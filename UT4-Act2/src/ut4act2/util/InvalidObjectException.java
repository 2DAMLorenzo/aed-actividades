package ut4act2.util;

/**
 * Created by TheL0w3R on 30/01/2019.
 * All Rights Reserved.
 */
public class InvalidObjectException extends Exception {
    public InvalidObjectException() {
        super("The specified object is not supported!");
    }
}
