package ut4act2.util;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Query;
import ut4act2.Department;
import ut4act2.Employee;

import java.lang.reflect.Array;

/**
 * Created by TheL0w3R on 30/01/2019.
 * All Rights Reserved.
 */
public class DBManager {

    private static DBManager instance;
    private ObjectContainer db;

    public DBManager() {
        instance = this;
        db = Db4oEmbedded.openFile(
                Db4oEmbedded.newConfiguration(), "db_departments_employees.yap");
    }

    public static <T> T[] queryAll(Class<T> type) {
        Query query = instance.db.query();
        query.constrain(type);
        ObjectSet<T> data = query.execute();
        return data.toArray((T[]) Array.newInstance(type, 0));
    }

    public static boolean store(Object object) throws InvalidObjectException {
        if(object instanceof Department) {
            Department toInsert = (Department) object;
            for(Department department : queryAll(toInsert.getClass())) {
                if(toInsert.getName().trim().equalsIgnoreCase(department.getName().trim()))
                    return false;
            }
        } else if(object instanceof Employee) {
            Employee toInsert = (Employee) object;

            for(Employee employee : queryAll(toInsert.getClass())) {
                if(toInsert.getDni().equalsIgnoreCase(employee.getDni()))
                    return false;
            }

            for(Department department : queryAll(Department.class)) {
                if(department.getName().trim().equalsIgnoreCase(toInsert.getDepartment().getName().trim())) {
                    toInsert.setDepartment(department);
                    break;
                }
            }
        } else
            throw new InvalidObjectException();

        instance.db.store(object);
        return true;
    }

    public static void store(Object... objects) throws InvalidObjectException {
        for(Object o : objects)
            store(o);
    }

    public static boolean update(Object object) throws InvalidObjectException {
        if(object instanceof Department) {
            Department department = (Department) object;
            Department[] departments = queryAll(Department.class);
            for(Department current : departments) {
                if(current.getName().trim().equalsIgnoreCase(department.getName().trim())) {
                    current.setName(department.getName());
                    current.setLocation(department.getLocation());
                    instance.db.store(current);
                    return true;
                }
            }
        } else if(object instanceof  Employee) {
            Employee employee = (Employee) object;
            Employee[] employees = queryAll(Employee.class);
            for(Employee current : employees) {
                if(current.getDni().trim().equalsIgnoreCase(employee.getDni().trim())) {
                    current.setDni(employee.getDni());
                    current.setName(employee.getName());
                    current.setSalary(employee.getSalary());
                    for(Department department : queryAll(Department.class)) {
                        if(employee.getDepartment().getName().trim().equalsIgnoreCase(department.getName().trim())) {
                            current.setDepartment(department);
                        }
                    }
                    instance.db.store(current);
                    return true;
                }
            }
        } else
            throw new InvalidObjectException();

        return false;
    }

    public static boolean delete(Object object) throws InvalidObjectException {
        if(object instanceof Department) {
            Department department = (Department) object;
            for(Department current : queryAll(Department.class)) {
                if(current.getName().trim().equalsIgnoreCase(department.getName().trim())) {
                    for(Employee employee : queryAll(Employee.class)) {
                        if(employee.getDepartment().getName().trim().equalsIgnoreCase(current.getName().trim()))
                            instance.db.delete(employee);
                    }
                    instance.db.delete(current);
                    return true;
                }
            }
        } else if(object instanceof  Employee) {
            Employee employee = (Employee) object;
            for(Employee current : queryAll(Employee.class)) {
                if(current.getDni().trim().equalsIgnoreCase(employee.getDni().trim())) {
                    Department empDepartment = current.getDepartment();
                    boolean isDepartmentEmpty = true;
                    for(Employee emp : queryAll(Employee.class)) {
                        if(empDepartment.getName().trim().equalsIgnoreCase(emp.getDepartment().getName().trim()) &&
                                !emp.getDni().trim().equals(current.getDni().trim()))
                            isDepartmentEmpty = false;
                    }
                    instance.db.delete(current);
                    if(isDepartmentEmpty)
                        instance.db.delete(empDepartment);
                    return true;
                }
            }
        } else
            throw new InvalidObjectException();

        return false;
    }
}
