package ut4act2;

import ut4act2.util.DBManager;
import ut4act2.util.InvalidObjectException;

public class Main {

    public static void main(String[] args) throws InvalidObjectException {
        new DBManager();
        System.out.println("Insertando 3 departamentos:");
        Department dep = new Department("Departamento castaña", "El Castaño");
        Department dep1 = new Department("Departamento ya lo iremos viendo", "El Castaño");
        Department dep2 = new Department("Departamento CIFP", "Villa de aguimes");

        DBManager.store(dep, dep1, dep2);

        System.out.println();
        printAll();
        System.out.println();

        System.out.println("Insertando 2 empleados por cada departamento:");
        Employee emp1 = new Employee("4335243F", "Joselito", 800, dep);
        Employee emp2 = new Employee("4332343A", "Anthony", 1000, dep);

        Employee emp3 = new Employee("7565365G", "Daniel", 3552, dep1);
        Employee emp4 = new Employee("3552654k", "Marcos", 680, dep1);

        Employee emp5 = new Employee("8568973I", "Pepe", 950, dep2);
        Employee emp6 = new Employee("1653674L", "Paco", 9785, dep2);

        DBManager.store(emp1, emp2, emp3, emp4, emp5, emp6);

        System.out.println();
        printAll();
        System.out.println();

        System.out.println("Mostrando empleados que trabajan en \"El Castaño\"");

        Employee[] employees = DBManager.queryAll(Employee.class);

        for(Employee e : employees)
            if(e.getDepartment().getLocation().equalsIgnoreCase("El Castaño"))
                System.out.println("  - " + e.getName() + " > Departamento: " + e.getDepartment().getName());

        System.out.println();
        System.out.println("Modificando el nombre de \"Anthony\" a \"Antonio\"");

        for(Employee e : employees) {
            if (e.getName().equalsIgnoreCase("Anthony")) {
                e.setName("Antonio");
                DBManager.update(e);
            }
        }

        System.out.println();
        printAll();
        System.out.println();

        System.out.println("Eliminando todos los empleados que cobran menos de 1000€");

        for(Employee e : employees)
            if (e.getSalary() < 1000)
                DBManager.delete(e);

        System.out.println();
        printAll();
    }

    private static void printAll() {
        System.out.println("Employees:");
        Employee[] employees = DBManager.queryAll(Employee.class);
        for(Employee e : employees)
            System.out.println("  - " + e.getName() + " > Departamento: " + e.getDepartment().getName());

        System.out.println("Departments:");
        Department[] departments = DBManager.queryAll(Department.class);
        for(Department d : departments)
            System.out.println("  - " + d.getName());
    }

}
