package ut4act2;

/**
 * Created by TheL0w3R on 30/01/2019.
 * All Rights Reserved.
 */
public class Employee {

    private String dni;
    private String name;
    private double salary;
    private Department department;

    public Employee(String dni, String name, double salary, Department department) {
        this.dni = dni;
        this.name = name;
        this.salary = salary;
        this.department = department;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }
}
